import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Doriti conversie din ROMANE IN ARABE SAU INVERS? R/A");
        java.util.Scanner s = new Scanner(System.in);
        Character ch2;
        do {
            System.out.print("tastati R, r, A sau a\t");
            ch2 = s.next().charAt(0);
        } while ((ch2 != 'R') && (ch2 != 'r') && (ch2 != 'A') && (ch2 != 'a'));

        if ((ch2 == 'r') || (ch2 == 'R')) {
            System.out.println("Introduceti nr. in cifre ROMANE =");
            String s2 = s.next();
            System.out.print(s2.toUpperCase() + " = ");
            s2 = s2.toLowerCase();
            char[] ch = s2.toCharArray();
            boolean romana = true;
            for (char ch1 : ch) {
                if (ch1 != ('i') && ch1 != ('v') && ch1 != ('x') && ch1 != ('l') && ch1 != ('c') && ch1 != ('d') && ch1 != ('m')) {
                    System.out.println("nu e cifra romana");
                    romana = false;
                }
            }
            int l = s2.length();
            int res = 0;
            if (romana == true) {
                while (l > 0) {
                    int lsb = NrRom.LSD(s2);
                    //System.out.print("LSD = " + lsb + "\t");
                    s2 = s2.substring(0, l - NrRom.c);
                    l = s2.length();
                    res = res + lsb;
                }
                System.out.print(" ");
                System.out.println(res);
            }
        }
            if ((ch2 == 'a') || (ch2 == 'A')) {
                System.out.printf("Introduceti nr. Arab = ");
                String str = s.next();
                int n = Integer.parseInt(str);
                System.out.println("nr.roman coresp.= " + NrArab.ArabToRomans(n));
            }
    }
}