public class NrArab {
    static String rez = " ";

    public static String ArabToRomans(int n) {
        if (n >= 1000) {
            int mii = n / 1000;
            while (mii > 0) {
                rez = rez + "M";
                mii--;
            }
            n = n % 1000;
        }
        if (n >= 100) {
            switch (n / 100) {
                case 0:
                    rez += "";
                    break;
                case 1:
                    rez += "C";
                    break;
                case 2:
                    rez += "CC";
                    break;
                case 3:
                    rez += "CCC";
                    break;
                case 4:
                    rez += "CD";
                    break;
                case 5:
                    rez += "D";
                    break;
                case 6:
                    rez += "DC";
                    break;
                case 7:
                    rez += "DCC";
                    break;
                case 8:
                    rez += "DCCC";
                    break;
                case 9:
                    rez += "CM";
                    break;
                default:
                    rez += "";
                    break;
            }
            n = n % 100;
        }
        if (n >= 10) {
            switch (n / 10) {
                case 0:
                    rez += "";
                    break;
                case 1:
                    rez += "X";
                    break;
                case 2:
                    rez += "XX";
                    break;
                case 3:
                    rez += "XXX";
                    break;
                case 4:
                    rez += "XL";
                    break;
                case 5:
                    rez += "L";
                    break;
                case 6:
                    rez += "LX";
                    break;
                case 7:
                    rez += "LXX";
                    break;
                case 8:
                    rez += "LXXX";
                    break;
                case 9:
                    rez += "XC";
                    break;
                default:
                    rez += "";
                    break;
            }
            n = n % 10;
        }
        if (n > 0) {
            switch (n) {
                case 0:
                    rez += "";
                    break;
                case 1:
                    rez += "I";
                    break;
                case 2:
                    rez += "II";
                    break;
                case 3:
                    rez += "III";
                    break;
                case 4:
                    rez += "IV";
                    break;
                case 5:
                    rez += "V";
                    break;
                case 6:
                    rez += "VI";
                    break;
                case 7:
                    rez += "VII";
                    break;
                case 8:
                    rez += "VIII";
                    break;
                case 9:
                    rez += "IX";
                    break;
                default:
                    rez += "";
                    break;
            }
        }
        return rez;
    }
}
