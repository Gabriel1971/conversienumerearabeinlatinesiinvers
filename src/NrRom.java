public class NrRom {
    String s;
    static int c;
    public static int LSD( String s){
        if (s.endsWith("i")&& !s.endsWith("ii")&& !s.endsWith("vi")) {   c=1;  return 1;}
        if (s.endsWith("ii")&& !s.endsWith("iii")&& !s.endsWith("vii")) {c = 2;  return 2;}
        if (s.endsWith("iii")&& !s.endsWith("viii"))    { c = 3;    return 3;}
        if (s.endsWith("vi")&& !s.endsWith("vii"))      { c = 2;    return 6;}
        if (s.endsWith("vii")&& !s.endsWith("viii"))    { c = 3;    return 7;}
        if (s.endsWith("viii"))                         { c = 4;    return 8;}
        if (s.endsWith("v")&& !s.endsWith("iv"))        { c = 1;    return 5;}
        if (s.endsWith("iv"))                           { c = 2;    return 4;}
        if (s.endsWith("ix"))                           { c=2;      return 9;}

        if (s.endsWith("x")&& !s.endsWith("xx")&& !s.endsWith("lx")&& !s.endsWith("ix")) {   c=1;  return 10;}
        if (s.endsWith("xx")&& !s.endsWith("lxx")&& !s.endsWith("xxx")) {c = 2;  return 20;}
        if (s.endsWith("xxx")&& !s.endsWith("lxxx"))    { c = 3;    return 30;}
        if (s.endsWith("lx")&& !s.endsWith("lxx"))      { c = 2;    return 60;}
        if (s.endsWith("lxx")&& !s.endsWith("lxxx"))    { c = 3;    return 70;}
        if (s.endsWith("lxxx"))                         { c = 4;    return 80;}
        if (s.endsWith("l")&& !s.endsWith("xl"))        { c = 1;    return 50;}
        if (s.endsWith("xl"))                           { c = 2;    return 40;}
        if (s.endsWith("xc"))                           { c = 2;    return 90;}

        if (s.endsWith("c")&& !s.endsWith("cc")&& !s.endsWith("xc")&& !s.endsWith("dc")) {   c=1;  return 100;}
        if (s.endsWith("cc")&& !s.endsWith("dcc")&& !s.endsWith("ccc")) {c = 2;  return 200;}
        if (s.endsWith("ccc")&& !s.endsWith("dccc"))    { c = 3;    return 300;}
        if (s.endsWith("dc")&& !s.endsWith("dcc"))      { c = 2;    return 600;}
        if (s.endsWith("dcc")&& !s.endsWith("dccc"))    { c = 3;    return 700;}
        if (s.endsWith("dccc"))                         { c = 4;    return 800;}
        if (s.endsWith("d")&& !s.endsWith("cd"))        { c = 1;    return 500;}
        if (s.endsWith("cd"))                           { c = 2;    return 400;}
        if (s.endsWith("cm"))                           { c = 2;    return 900;}

        if (s.endsWith("m")&& !s.endsWith("cm"))        { c = 1;    return 1000;}

        return 0;
    }
}
